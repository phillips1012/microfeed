/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const gulp = require('gulp');

const del = require('del');
const babel = require('gulp-babel');
const browserify = require('browserify');
const envify = require('envify');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const jade = require('gulp-jade');
const stylus = require('gulp-stylus');
const nib = require('nib');

const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');

gulp.task('clean-unneeded', function(cb) {
  del([
    'libbrowser'
  ], cb);
});

gulp.task('clean', ['clean-unneeded'], function(cb) {
  del([
    'lib', 'dist'
  ], cb);
});

gulp.task('deep-clean', ['clean'], function(cb) {
  del([
    'node_modules', 'rethinkdb_data'
  ], cb);
});

gulp.task('babel', function() {
  return merge(
    // first, we compile browser-compatible sources to
    // `libbrowser`. these sources will have slightly more
    // transforms in order to be compatible with browsers
    gulp.src('src/**/*.js*')
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(babel({
        stage: 0,
        optional: [
          'runtime'
        ],
        blacklist: [
        ]
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('libbrowser')),

    // then, we compile server sources to `lib`
    gulp.src('src/**/*.js*')
      .pipe(sourcemaps.init())
      .pipe(babel({
        stage: 0,
        optional: [
          'runtime', 'bluebirdCoroutines'
        ],
        blacklist: [
          'regenerator', 'es6.constants'
        ]
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('lib'))
  );
});

gulp.task('babel-watch', function() {
  gulp.watch('src/**/*.js*', ['babel']);
});

gulp.task('jade', function() {
  return gulp.src('res/*.jade')
    .pipe(jade({
      locals: {
        config: require('./clientConfig')
      }
    }))
    .pipe(gulp.dest('dist/public'));
});

gulp.task('jade-watch', function() {
  gulp.watch('res/*.jade', ['jade']);
});

gulp.task('browserify', ['babel'], function() {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';

  // libbbrowser should be pre-populated with babel's output
  var stream = browserify('libbrowser/client/index.js', {
    debug: true
  })
    .transform(envify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }));

  if (process.env.NODE_ENV === 'production') {
    stream = stream
      .pipe(uglify());
  }

  return stream
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/public'));
});

gulp.task('browserify-watch', function() {
  gulp.watch('src/**/*.js*', ['browserify']);
});

gulp.task('stylus', function() {
  return gulp.src('./styles/main.styl')
    .pipe(sourcemaps.init())
    .pipe(stylus({
      compress: process.env.NODE_ENV === 'production',
      use: nib()
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/public'));
});

gulp.task('stylus-watch', function() {
  gulp.watch('styles/**/*.styl', ['stylus']);
});

gulp.task('default', ['stylus', 'browserify', 'jade']);
