/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('main');
import config from '../../clientConfig';

import React from 'react/addons';
import Router, { Route } from 'react-router';
import alt from './alt';

import './i18n';

import Base from './components/Base';
import Register from './components/Register';
import NotFound from './components/NotFound';

if (process.env.NODE_ENV === 'development') {
  console.log('warning: in development mode');
  window.debug = require('debug');
  window.debug.enable('*');
}

window.addEventListener('load', event => {
  Router.run(<Route path="/" handler={Base}>
    <Router.NotFoundRoute handler={NotFound} />
    <Route path="register" name="register" handler={Register} />
  </Route>, Router.HashLocation, (Root) => {
    React.render(<Root />, document.body);
  });
});
