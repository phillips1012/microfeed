/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('Register');
import config from '../../../clientConfig';

import React from 'react/addons';
import translate from 'counterpart';
import joi from 'joi';
import Section from './Section';

export default class Register extends React.Component {
  render() {
    return <Section>
      <form className="register-form">
        <div>
          <label htmlFor="username">{translate('user_fields.username')}: </label>
          <input type="text" valueLink={this.linkState('username')}></input>
        </div>
        <div>
          <label htmlFor="password">{translate('user_fields.password')}: </label>
          <input type="password" valueLink={this.linkState('password')}></input>
        </div>
        <div>
          <label htmlFor="email">{translate('user_fields.email')}: </label>
          <input type="email" valueLink={this.linkState('Email')} />
        </div>
        <input type="submit" value={translate('actions.register')} />
      </form>
    </Section>;
  }

  state = {};
}

require('react-mixin')(Register.prototype, React.addons.LinkedStateMixin);
