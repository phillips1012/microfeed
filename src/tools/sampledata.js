/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('sample data tool');

import Chance from 'chance';
import User from '../server/models/User.js';

const chance = new Chance();

setTimeout(() => {
  for (let i = 0; i < 100; i++) {
    const username = `${chance.bool() ? chance.first() : chance.word()}_${
      chance.character({ pool: '0123456789' })
    }`;

    const password = chance.string({
      length: chance.natural({ min: 5, max: 20 }),
      pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789!@#$%^&*()'
    });

    console.log(`user ${username} has password ${password}`);

    const user = new User({
      username: username
    });

    user.savePassword(password).then(hashed => {
      user.save();
    });
  }
}, 2000);
