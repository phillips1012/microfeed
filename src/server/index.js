/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('app');

import Application from 'koa';
import Router from 'koa-router';
import * as middleware from './middleware';
import * as user from './controllers/user';
import * as schemas from '../formSchemas';

debug('init koa');
export const app = new Application();

app.use(middleware.log);
app.use(middleware.staticFiles);
app.use(middleware.error);

const router = new Router();

router.post('/api/key',
  middleware.parseJSON(schemas.login),
  user.login);

router.delete('/api/key',
  user.authenticate(false),
  user.logout);

router.post('/api/user',
  middleware.parseJSON(schemas.register),
  user.register);

app.use(router.routes());
app.use(router.allowedMethods());
