/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('db');

import config from '../../config';
import validator from 'validator';
import { thinky, type, r } from './db';

/**
 * Generate a thinky model from a class
 * @param model anonymous class to use
 * @return thinky model
 */
export function model(model) {
  debug(`initializing ${model.table}`);

  const ret = thinky.createModel(model.table, model.schema);

  Object.keys(model.indices).map((index) => {
    debug(`registering index ${index}`);
    ret.ensureIndex(index, model.indices[index]);
  });

  model.staticMethods.map(methodName => {
    debug(`registering static method ${methodName}`);
    ret.defineStatic(methodName, model[methodName]);
  });

  model.methods.map(methodName => {
    debug(`registering method ${methodName}`);
    ret.define(methodName, model.prototype[methodName]);
  });

  return ret;
}

export const schemaMixins = {
  date: {
    /**
     * Date
     */
    date: type.date().required().default(r.now())
  },

  ip: {
    /**
     * IP address
     */
    ip: type.string()
      .validator(validator.isIP)
  }
};
