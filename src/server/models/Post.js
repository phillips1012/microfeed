/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('user');

import flake from 'simpleflake';
import { type } from '../db';
import { model, schemaMixins } from '../modelFactory';

export default model(class Post {
  static table = 'post';

  static schema = {
    /**
     * Alphanumeric ID
     */
    id: type.string().required()
      .default(function() {
        return flake().toString('base58');
      }),

    /**
     * Poster
     */
    posterID: type.string().required(),

    /**
     * Post title
     */
    title: type.string().required().default(''),

    /**
     * Post body
     */
    body: type.string().required().default(''),

    /**
     * Post URL
     */
    url: type.string(),

    /**
     * Post tags, key is the tag name, value is `true`
     */
    tags: type.object().required().default({}),

    ...schemaMixins.date,
    ...schemaMixins.ip
  };
});
