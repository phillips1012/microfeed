/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('user');

import config from '../../../config';
import Promise from 'bluebird';
import bcrypt from 'bcrypt';
import Chance from 'chance';
import { type } from '../db';
import { model, schemaMixins } from '../modelFactory';

const chance = new Chance();

export default model(class User {
  static table = 'user';

  static schema = {
    /**
     * Lowercase canonical username
     */
    id: type.string().required()
      .default(function() {
        return this.username.toLowerCase();
      }),

    /**
     * Case-preserving username
     */
    username: type.string().required(),

    /**
     * Group this user is a member of
     */
    // group: type.string().required()
    //   .default(config.defaultGroup),

    /**
     * Email verification key
     */
    emailKey: type.string(),
    /**
     * Is the email address verified
     */
    emailVerified: type.boolean().required()
      .default(true),

    /**
     * Password, as bcrypt
     */
    password: type.string().required(),

    /**
     * Email address
     */
    email: type.string().email()
      .max(500),

    /**
     * Profile text
     */
    profile: type.string(),

    /**
     * Profile picture
     */
    picture: type.buffer(),

    /**
     * Is banned
     */
    banned: type.boolean().default(false),

    ...schemaMixins.date,
    ...schemaMixins.ip
  };

  static indices = {
    email: undefined,
    date: undefined
  };

  static staticMethods = [
    'checkAvailable'
  ];

  static methods = [
    'checkPassword', 'savePassword', 'genEmailKey', 'checkAvailable'
  ];

  /**
   * Check if username/email combination is taken
   * @param username
   * @param email
   * @return {Promise<Object>} { username: Boolean, password: Boolean }
   */
  static checkAvailable(username: String, email: String): Promise {
    return Promise.props({
      name: this.getAll(username.toLowerCase()).run(),
      email: this.getAll(email, { index: 'email' }).run()
    }).then(results => {
      return {
        username: results.name.length !== 0,
        email: results.email.length !== 0
      };
    });
  }

  /**
   * Check if password is `password`
   * @param password password to Check
   * @return {Promise<Boolean>}
   */
  checkPassword(password: String): Promise {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, this.password, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
  }

  /**
   * Saves password
   * @param password
   * @return {Promise<String>}
   */
  savePassword(password: String): Promise {
    return new Promise((resolve, reject) => {
      debug('bcrypt init');
      bcrypt.hash(password, config.passwords.bcryptStrength, (err, res) => {
        debug('bcrypt end');
        if (err) return reject(err);
        this.password = res;
        resolve(res);
      });
    });
  }

  /**
   * Generate email key
   */
  genEmailKey(): User {
    this.emailVerified = false;
    this.emailKey = chance.string({
      pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      length: 24
    });
  }
});
