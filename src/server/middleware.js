/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('middleware');

import logger from 'koa-logger';
import serve from 'koa-static';
import parse from 'co-body';
import joi from 'joi';
import * as errors from '../errors';
import httpError from 'http-errors';

export const log = logger();
export const staticFiles = serve('./dist/public');

export function parseJSON(schema: Object): Function {
  return function*(next) {
    if (!this.is('json')) {
      throw httpError(415, undefined, { errors: [
        new error.BadContentType()
      ] });
    }

    try {
      this.state.data = yield parse.json(this);
    } catch(err) {
      if (err instanceof SyntaxError) {
        debug('json invalid');
        throw httpError(422, undefined, { errors: [
          new errors.BadJson()
        ] });
      } else {
        throw err;
      }
    }
    const { value, error } = joi.validate(this.state.data, schema, {
      stripUnknown: true
    });

    if (debug.enabled) {
      const before = { ...this.state.data };
      if (before.password) before.password = '******';
      const after = { ...value };
      if (after.password) after.password = '******';

      debug('transformed', before, after);
    }

    if (error == null) {
      debug('json valid');
      this.state.data = value;
      yield next;
    } else {
      debug('json invalid', error);
      throw httpError(422, undefined, { errors: [
        new errors.BadJson(error.details)
      ] });
    }
  };
}

export function* error(next) {
  try {
    yield next;
  } catch (err) {
    if ((typeof err.status === 'number')
        && (err.errors instanceof Array)) {
      this.status = err.statusCode;
      this.body = {
        errors: err.errors.map(error => {
          return { ...error };
        })
      };
    } else {
      throw err;
    }
  }
}
