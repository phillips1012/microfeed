/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const debug = require('debug')('user controller');

import config from '../../../config';
import Promise from 'bluebird';
import User from '../models/User';
import cryptonode from 'crypto';
import httpError from 'http-errors';
import * as errors from '../../errors';
import { redis, thinky } from '../db';

const crypto = Promise.promisifyAll(cryptonode);

export function authenticate(needUser: Boolean) {
  return function*(next) {
    const deny = () => {
      throw httpError(401, undefined, { errors: [
        new errors.BadAuth()
      ] });
    };

    if (this.header.authorization == null) return deny();

    const [authtype, key] = this.header.authorization.split(' ');

    if (authtype.toLowerCase() !== 'login-key') return deny();

    const username = yield redis.get(`session:${key}`);
    debug(username == null ? `key is dead` : `key owned by ${username}`);

    if (username == null) {
      throw httpError(401, undefined, { errors: [
        new errors.DeadKey()
      ] });
    }

    this.state.key = key;
    if (needUser) {
      this.state.user = yield User.get(username.toLowerCase());
    }

    yield next;
  };
}

export function* login() {
  debug('enter login route');

  const deny = () => {
    throw httpError(401, undefined, { errors: [
      new errors.BadCredentials()
    ] });
  };

  const { data } = this.state;

  try {
    const user = yield User.get(data.username);
    const correctPassword = yield user.checkPassword(data.password);

    debug(`password is ${correctPassword ? '' : 'in'}correct`);

    if (correctPassword) {
      const sessionKey = (yield crypto.randomBytesAsync(128)).toString('hex');
      yield redis.setex(`session:${sessionKey}`, config.passwords.sessionExpire,
        user.username);
      this.body = {
        key: sessionKey
      };
    } else {
      deny();
    }
  } catch (err) {
    if (err instanceof thinky.Errors.DocumentNotFound) {
      return deny();
    } else {
      throw err;
    }
  }
}

export function* logout() {
  debug('enter logout route');

  yield redis.del(`session:${this.state.key}`);
  this.body = {
    success: true
  };
}

export function* register() {
  debug('enter register route');

  const { data } = this.state;
  const taken = yield User.checkAvailable(data.username, data.email);
  if (taken.username || taken.email) {
    throw httpError(409, undefined, { errors: [
      new errors.Taken(undefined, taken)
    ] });
  }

  const user = new User({
    username: data.username,
    email: data.email,
    ip: this.ip
  });

  yield user.savePassword(data.password);
  yield user.save();

  this.body = {
    success: true
  };
}
