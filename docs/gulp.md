gulp tasks
==========

Microfeed uses gulp for executing tasks.

### `gulp clean-unneeded`

Deletes `libbrowser`, which is not needed for server runtime.

### `gulp clean`

Deletes `lib`, `dist`, and `libbrowser`.

### `gulp deep-clean`

Deletes `rethinkdb_data`, `node_modules`, `lib`, `dist`, `libbrowser`

### `gulp babel`

Compiles `src` to `lib` and `libbrowser`. `libbrowser` does not contain client-only code, it is exactly the same as `lib` except it uses a slightly different babel configuration in order to work with browsers. This way, we can specify different configurations so that the server code can take advantage of node's `--harmony` performance benefits over babel's regenerator runtime.

### `gulp babel-watch`

Watches `src` and runs `babel`.

### `gulp jade`

Compiles jade templates in `res` to `dist/public`

### `gulp jade-watch`

Watches `res/*.jade` and runs `jade`.

### `gulp browserify`

Bundles `libbrowser/client/app.js` to `public/dist/bundle.js`

### `gulp browserify-watch`

Watches `src` and runs `browserify`.
