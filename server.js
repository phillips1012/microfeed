/*
 * This file is a component of microfeed
 *
 * Copyright (c) 2015 phillips1012
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

'use strict';

var processes = require('os').cpus().length;

const debug = require('debug')('server');
const cluster = require('cluster');
const argv = require('minimist')(process.argv);
const config = require('./config');

if (typeof argv.processes === 'number') processes = argv.processes;

if (cluster.isMaster) {
  debug('spawning ' + processes + ' workers');
  while (processes--) cluster.fork();

  cluster.on('exit', worker => {
    debug('worker ' + worker.process.pid + ' died, replacing in 10 seconds');
    setTimeout(cluster.fork.bind(cluster), 10000);
  });
} else {
  const path = require('path');
  const http = require('http');
  const app = require('./lib/server').app;

  debug('worker ' + process.pid + ' started');

  app.listen(config.http.port, config.http.host);
}

(() => 'Please enable --harmony')();
